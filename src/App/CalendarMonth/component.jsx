/* eslint-disable */
import React from 'react';
import moment from 'moment';
import './styles.scss';


const CalendarMonth = ({ nowRef, monthIndex, year, holidaysByDay, handleDayClick }) => {
    const now = nowRef.current;
    const todayInMonth = now.month() === monthIndex && now.year() === year;
    const firstDayOfMonth = now.clone().year(year).month(monthIndex).startOf('month');
    let firstDayModifier = 0;
    const firstWeekdayOfMonth = () => {
        const dayIsSun = firstDayOfMonth.day() === 0;
        const dayIsSat = firstDayOfMonth.day() === 6;
        if (dayIsSat) {
            firstDayModifier = 2;
            return firstDayOfMonth.clone().add(1, 'day').day(1)
        } else if (dayIsSun) {
            firstDayModifier = 1;
            return firstDayOfMonth.clone().add(1, 'day').day(1)
        } else {
            return firstDayOfMonth
        }
    }
    const firstWeekdayIndex = firstWeekdayOfMonth().day();
    const daysInMonth = firstDayOfMonth.daysInMonth();

    let weekdays = moment.weekdaysMin(); // ['Su', 'Mo', ..]

    return (
        <div className="month">
            <div className="monthName">{moment().month(monthIndex).format('MMMM')}</div>
            <div className="grid">
                {weekdays.map(day => {
                    return <div className="day header" key={day}>{day[0]}</div>
                }
                )}
                {[...Array(5 * 7)].map((x, gridIndex) => {
                    let isEmpty = true;
                    const dayNumber = gridIndex - firstWeekdayIndex + 1 + firstDayModifier;
                    if (dayNumber - 1 >= 0 && dayNumber <= daysInMonth) {
                        isEmpty = false;
                    }
                    const dayText = isEmpty ? '' : dayNumber.toString();

                    let classArray = isEmpty ? ['empty'] : [''];
                    const holidayObj = holidaysByDay[dayNumber]
                    if (holidayObj) {
                        if (holidayObj.isBankHoliday) {
                            classArray.push('bankholiday');
                        } else {
                            if (holidayObj.inFuture) {
                                classArray.push('booked');
                            } else {
                                classArray.push('used');
                            }
                            if (holidayObj.isHalfday) {
                                classArray.push('halfday');
                            }
                            if (holidayObj.isPlanning) {
                                classArray.push('planning');
                            }
                        }

                    }
                    if (todayInMonth && dayNumber === now.date()) {
                        classArray.push('today');
                    }
                    return (
                        <div
                            key={gridIndex}
                            className={`day ${classArray.join(' ')}`}
                            onClick={(e) => {
                                handleDayClick({ e, month: monthIndex+1, day: dayNumber })
                            }}
                        >
                            <div className="bg"></div>
                            {dayText}
                        </div>
                    );
                })}
            </div>
        </div>
    )
};

export default CalendarMonth;
