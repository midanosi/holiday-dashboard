import React, { useState, useRef, useEffect } from 'react';

import * as CSV from 'csv-string';
import moment from 'moment';

import { Icon } from '@iconify/react';
import arrowLeftAlt2 from '@iconify/icons-dashicons/arrow-left-alt2';
import arrowRightAlt2 from '@iconify/icons-dashicons/arrow-right-alt2';


import CalendarMonth from './CalendarMonth';
import UsageStats from './UsageStats';
import './styles.scss';


function App() {
    const nowRef = useRef(moment());
    const [year, setYear] = useState(2021);

    const [showBankHolidays] = useState(true);
    const [showCancelled] = useState(false);
    // const totalHolidayEntitlement = 25;
    // const totalBankHolidayEntitlement = 7;

    const [holidaysByDate, setHolidaysByDate] = useState({});
    const handleDayClick = ({ e, month, day }) => {
        const dateKey = `${day.toString().padStart(2, '0')}/${month.toString().padStart(2, '0')}/${year}`;
        // if (moment(dateKey, 'DD/MM/YYYY').isBefore(nowRef.current)) {
        //     alert('Selected day is in the past');
        //     return;
        // }

        const dummyHolidaysByDate = { ...holidaysByDate };
        if (dummyHolidaysByDate[dateKey] && dummyHolidaysByDate[dateKey].status === 'Planning') {
            delete dummyHolidaysByDate[dateKey];
        } else {
            dummyHolidaysByDate[dateKey] = {
                status: 'Planning',
                isPlanning: true,
            }
            if (e.shiftKey) {
                dummyHolidaysByDate[dateKey].isHalfday = true;
            }
        }
        setHolidaysByDate(dummyHolidaysByDate);
    }
    const [holidaysByYearMonthDay, setHolidaysByYearMonthDay] = useState(new Array(12).fill(0).map(() => ({})));

    const [usageByYear, setUsageByYear] = useState({
        // '2018': {
        //     totalHolidayEntitlement: 0,
        //     totalBankHolidayEntitlement: 0,
        //     daysUsed: 0,
        //     daysBooked: 0,
        //     daysBHUsed: 0,
        //     daysBHBooked: 0,
        // },
        '2019': {
            totalHolidayEntitlement: 23.5,
            totalBankHolidayEntitlement: 7,
            daysUsed: 0,
            daysBooked: 0,
            daysPlanned: 0,
            daysBHUsed: 0,
            daysBHBooked: 0,
        },
        '2020': {
            totalHolidayEntitlement: 25 + 2.5, // 2.5 carried over from last year
            totalBankHolidayEntitlement: 8,
            daysUsed: 0,
            daysBooked: 0,
            daysPlanned: 0,
            daysBHUsed: 0,
            daysBHBooked: 0,
        },
        '2021': {
            totalHolidayEntitlement: 25 + 5, // 5 carried over from last year
            totalBankHolidayEntitlement: 8,
            daysUsed: 0,
            daysBooked: 0,
            daysPlanned: 0,
            daysBHUsed: 0,
            daysBHBooked: 0,
        },
        '2022': {
            totalHolidayEntitlement: 4 + 6, // 6 carried over from last year, holiday only for two months
            totalBankHolidayEntitlement: 1,
            daysUsed: 0,
            daysBooked: 0,
            daysPlanned: 0,
            daysBHUsed: 0,
            daysBHBooked: 0,
        },
    })
    // const [daysUsed, setDaysUsed] = useState(0);
    // const [daysBooked, setDaysBooked] = useState(0);
    // const [daysBHUsed, setBHDaysUsed] = useState(0);
    // const [daysBHBooked, setBHDaysBooked] = useState(0);

    const textareaRef = useRef();


    const scrapeHolidayRequests = () => {
        const textarea = textareaRef.current;
        const parsed = CSV.parse(textarea.value, ' ');
        let dummyHolidaysByDate = {};

        parsed.forEach(req => {
            const reqString = req.join('');
            let reqArray = reqString.replace(/\s+|\t+/g, ',').split(',')
            if (reqArray[0] === '') {
                reqArray = reqArray.slice(2);
            } else {
                reqArray = reqArray.slice(1);

            }
            const startDate = moment(reqArray[0], 'DD/MM/YYYY');
            const duration = reqArray[2];
            const status = reqArray[3];

            const dateToIterate = startDate.clone();
            for (let i = 0; i < duration; i++) {
                const dayEntry = {
                    status: status === 'Bankholiday' ? 'Approved' : status,
                };
                if (duration === '0.5') { dayEntry.isHalfday = true }
                if (status === 'Bankholiday') { dayEntry.isBankHoliday = true }
                if (status === 'Planning') { dayEntry.isPlanning = true }

                const daysToJump = dateToIterate.day() === 5 ? 3 : 1;
                dummyHolidaysByDate[dateToIterate.format('DD/MM/YYYY')] = dayEntry;
                dateToIterate.add(daysToJump, 'days');
            }
        });
        // console.log('dummyHolidaysByDate', dummyHolidaysByDate);
        setHolidaysByDate(dummyHolidaysByDate);
    }

    useEffect(() => {
        scrapeHolidayRequests();
    }, []);

    useEffect(() => {
        let dummyDaysBooked = 0;
        let dummyDaysPlanned = 0;
        let dummyDaysUsed = 0;
        let dummyBHDaysBooked = 0;
        let dummyBHDaysUsed = 0;
        let dummyHolidaysByYearMonthDay = {
            // '2018': new Array(12).fill(0).map(() => ({})),
            '2019': new Array(12).fill(0).map(() => ({})),
            '2020': new Array(12).fill(0).map(() => ({})),
            '2021': new Array(12).fill(0).map(() => ({})),
            '2022': new Array(12).fill(0).map(() => ({})),
        }

        for (let date in holidaysByDate) {
            const req = holidaysByDate[date];
            // if (!showBankHolidays && req.isBankHoliday) {
            //     continue;
            // }
            if (!showCancelled && req.status === 'Cancelled') {
                continue;
            }

            const dayLength = req.isHalfday ? 0.5 : 1;
            const dayInFuture = moment(date, 'DD/MM/YYYY').isAfter(nowRef.current, 'days');
            if (moment(date, 'DD/MM/YYYY').year() === year) {
                if (dayInFuture) {
                    if (req.isBankHoliday) {
                        dummyBHDaysBooked += dayLength;
                    } else if (req.isPlanning) {
                        dummyDaysPlanned += dayLength;
                    } else {
                        dummyDaysBooked += dayLength;
                    }
                } else {
                    if (req.isBankHoliday) {
                        dummyBHDaysUsed += dayLength;
                    } else {
                        dummyDaysUsed += dayLength;
                    }
                }
            }

            const yearIndex = moment(date, 'DD/MM/YYYY').year();
            const monthIndex = moment(date, 'DD/MM/YYYY').month();
            const dayObject = {
                inFuture: dayInFuture,
            }
            if (req.isHalfday) { dayObject.isHalfday = req.isHalfday }
            if (req.isBankHoliday) { dayObject.isBankHoliday = req.isBankHoliday }
            if (req.isPlanning) { dayObject.isPlanning = req.isPlanning }

            dummyHolidaysByYearMonthDay[yearIndex][monthIndex][moment(date, 'DD/MM/YYYY').date()] = dayObject;

        }
        setUsageByYear(usage => ({
            ...usage,
            [year]: {
                ...usage[year],
                daysUsed: dummyDaysUsed,
                daysBooked: dummyDaysBooked,
                daysPlanned: dummyDaysPlanned,
                daysBHUsed: dummyBHDaysUsed,
                daysBHBooked: dummyBHDaysBooked,
            },
        }))
        // setDaysUsed(dummyDaysUsed)
        // setDaysBooked(dummyDaysBooked);
        // setBHDaysUsed(dummyBHDaysUsed);
        // setBHDaysBooked(dummyBHDaysBooked);

        setHolidaysByYearMonthDay(dummyHolidaysByYearMonthDay);
        // console.log(dummyHolidaysByYearMonthDay);

    }, [holidaysByDate, showBankHolidays, showCancelled, year]);


    return (
        <div className="app">
            <div className="data-entry-section">
                <h3 className="subtitle">Data Entry</h3>
                <textarea
                    id="requestsTablePasteArea"
                    ref={textareaRef}
                    defaultValue={`	Document complete Icon	27/12/2019	31/12/2019	3	Approved		
                    Calendar Icon	26/12/2019	26/12/2019	1	Bank holiday		
                Calendar Icon	25/12/2019	25/12/2019	1	Bank holiday		
                    Document complete Icon	23/12/2019	24/12/2019	2	Approved		
                Document complete Icon	02/12/2019	03/12/2019	2	Approved		
                    Document complete Icon	29/11/2019	29/11/2019	0.5	Approved		
                Document complete Icon	23/09/2019	30/09/2019	6	Approved		
                    Calendar Icon	26/08/2019	26/08/2019	1	Bank holiday		
                Document complete Icon	26/07/2019	26/07/2019	1	Approved		
                    Document complete Icon	25/07/2019	25/07/2019	1	Approved		
                Document complete Icon	08/07/2019	09/07/2019	2	Approved		
                    Document complete Icon	13/06/2019	14/06/2019	2	Approved		
                Calendar Icon	27/05/2019	27/05/2019	1	Bank holiday		
                    Calendar Icon	06/05/2019	06/05/2019	1	Bank holiday		
                Document complete Icon	03/05/2019	03/05/2019	1	Approved		
                    Calendar Icon	22/04/2019	22/04/2019	1	Bank holiday		
                Calendar Icon	19/04/2019	19/04/2019	1	Bank holiday		
                    Document problem Icon	18/03/2019	22/03/2019	5	Cancelled		
                Document complete Icon	19/02/2019	19/02/2019	0.5	Approved		
                Hide details for 20202020	
                Calendar Icon	1/1/2020	1/1/2020	1	Bank holiday	
                Calendar Icon	28/12/2020	28/12/2020	1	Bank holiday		
                    Calendar Icon	25/12/2020	25/12/2020	1	Bank holiday		
                Calendar Icon	31/08/2020	31/08/2020	1	Bank holiday		
                    Calendar Icon	25/05/2020	25/05/2020	1	Bank holiday		
                Calendar Icon	08/05/2020	08/05/2020	1	Bank holiday		
                    Calendar Icon	13/04/2020	13/04/2020	1	Bank holiday		
                Calendar Icon	10/04/2020	10/04/2020	1	Bank holiday	
                
                Document problem Icon	30/03/2020	03/04/2020	5	Cancelled		
                Document complete Icon	21/02/2020	21/02/2020	1	Approved		
                Document complete Icon	20/01/2020	24/01/2020	5	Approved	
                Document complete Icon	12/06/2020	15/06/2020	2	Approved	
                Document complete Icon	15/05/2020	15/05/2020	1	Approved
                
                Document complete Icon	23/07/2020	23/07/2020	1	Approved
                Document complete Icon	07/08/2020	07/08/2020	1	Approved
                Document complete Icon	19/08/2020	21/08/2020	3	Approved
                Document complete Icon	14/09/2020	16/09/2020	3	Approved
                Document complete Icon	17/09/2020	17/09/2020	0.5	Approved
                Document complete Icon	23/12/2020	24/12/2020	2	Approved
                Document complete Icon	29/12/2020	31/12/2020	3	Approved

                Document complete Icon	22/02/2021	24/02/2021	3	Approved
                Document complete Icon	29/03/2021	01/04/2021	4	Approved
                Document complete Icon	14/05/2021	14/05/2021	1	Approved
                Document complete Icon	21/05/2021	21/05/2021	1	Approved
                Document complete Icon	14/06/2021	15/06/2021	2	Approved
                Document complete Icon	26/07/2021	28/07/2021	3	Approved
                Document complete Icon	10/09/2021	10/09/2021	1	Approved

                Document complete Icon	01/10/2021	01/10/2021	1	Approved
                Document complete Icon	25/10/2021	29/10/2021	5	Approved
                Document complete Icon	23/12/2021	24/12/2021	2	Planning
                Document complete Icon	29/12/2021	29/12/2021	1	Planning
                
                Document complete Icon	17/01/2022	21/01/2022	5	Planning

                Document complete Icon	21/02/2022	25/02/2022	5	Planning

                Calendar Icon	1/1/2021	1/1/2021	1	Bank holiday	
                Calendar Icon	28/12/2021	28/12/2021	1	Bank holiday		
                    Calendar Icon	27/12/2021	27/12/2021	1	Bank holiday		
                Calendar Icon	30/08/2021	30/08/2021	1	Bank holiday		
                    Calendar Icon	31/05/2021	31/05/2021	1	Bank holiday		
                Calendar Icon	03/05/2021	03/05/2021	1	Bank holiday		
                    Calendar Icon	05/04/2021	05/04/2021	1	Bank holiday		
                Calendar Icon	02/04/2021	02/04/2021	1	Bank holiday

                Calendar Icon	3/1/2022	3/1/2022	1	Bank holiday
                `
                    }
                >
                </textarea>
                <button onClick={() => scrapeHolidayRequests()}>
                    Scrape holiday requests
                </button>
            </div>
            <div className="stats-section">
                <h3 className="subtitle">Stats</h3>
                <UsageStats
                    usageObj={usageByYear[year]}
                />

            </div>
            <div className="calendar-section">
                <h3 className="subtitle">Calendar</h3>
                <div className="year-wrapper">
                    <div className="control" onClick={() => setYear(y => y-1)}><Icon icon={arrowLeftAlt2} /></div>
                    <div className="year">{year}</div>
                    <div className="control" onClick={() => setYear(y => y+1)}><Icon icon={arrowRightAlt2} /></div>
                </div>
                <div className="calendar">
                    {holidaysByYearMonthDay[year] &&
                        holidaysByYearMonthDay[year].map((holidaysByDay, i) => {
                            return (
                                <CalendarMonth
                                    key={i}
                                    nowRef={nowRef}
                                    monthIndex={i}
                                    year={year}
                                    holidaysByDay={holidaysByDay}
                                    handleDayClick={handleDayClick}
                                />
                            )
                        })}
                </div>
            </div>
        </div>
    );
}

export default App;
