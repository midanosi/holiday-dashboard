import React from 'react';
import './styles.scss';


const numFormat = num => {
    const int = parseInt(num);
    const halfStringVisible = (num % 1 === 0.5);
    const halfString = '½';

    return (
        <div className="numFormatted">
            {int}
            <div className={`halfString ${halfStringVisible ? 'visible' : ''}`}>{halfString}</div>
        </div>
    );
};
const UsageStats = ({ usageObj }) => {
    const totalHolidayEntitlement = usageObj.totalHolidayEntitlement;
    const totalBankHolidayEntitlement = usageObj.totalBankHolidayEntitlement;
    const daysUsed = usageObj.daysUsed;
    const daysBooked = usageObj.daysBooked;
    const daysPlanned = usageObj.daysPlanned;
    const daysBHUsed = usageObj.daysBHUsed;
    const daysBHBooked = usageObj.daysBHBooked;
    
    return (
        <div className="usageStatsWrapper">
            <div className="table">
                <div className="header"></div>
                <div className="header">base</div>
                <div className="header">w/ BH</div>
                <div className="total title">Total</div>
                <div className="total">{numFormat(totalHolidayEntitlement)}</div>
                <div className="total">{numFormat(totalHolidayEntitlement+totalBankHolidayEntitlement)}</div>
                <div className="used title">Used</div>
                <div className="used">{numFormat(daysUsed)}</div>
                <div className="used bh">{numFormat(daysUsed+daysBHUsed)}</div>
                <div className="booked title">Booked</div>
                <div className="booked">{numFormat(daysBooked)}</div>
                <div className="booked bh">{numFormat(daysBooked+daysBHBooked)}</div>
                <div className="planned title">Planned</div>
                <div className="planned">{numFormat(daysPlanned)}</div>
                <div className="planned bh">{numFormat(daysPlanned+daysBHBooked)}</div>
                <div className="remaining title">Remaining</div>
                <div className="remaining">{numFormat(totalHolidayEntitlement-daysUsed-daysBooked-daysPlanned)}</div>
                <div className="remaining bh">
                    {numFormat(totalHolidayEntitlement+totalBankHolidayEntitlement-daysUsed-daysBHUsed-daysBooked-daysBHBooked-daysPlanned)}
                </div>
            </div>
            <div className="visualisation">
                {[...Array(parseInt(daysUsed))].map((x, i) =>
                    <div key={i} className="square used"><div className="bg"/></div>,
                )}
                {(daysUsed % 1) > 0 &&
                    <div className="square used half"><div className="bg"/></div>
                }
                {[...Array(parseInt(daysBooked))].map((x, i) =>
                    <div key={i} className="square booked"><div className="bg"/></div>,
                )}
                {(daysBooked % 1) > 0 ?
                    (<div className="square booked half"><div className="bg"/></div>) : null
                }
                {[...Array(parseInt(daysPlanned))].map((x, i) =>
                    <div key={i} className="square planned"><div className="bg"/></div>,
                )}
                {(daysPlanned % 1) > 0 ?
                    (<div className="square planned half"><div className="bg"/></div>) : null
                }
                {[...Array(parseInt(totalHolidayEntitlement-daysUsed-daysBooked-daysPlanned))].map((x, i) =>
                    <div key={i} className="square remaining"><div className="bg"/></div>,
                )}
                {((totalHolidayEntitlement-daysUsed-daysBooked-daysPlanned) % 1) > 0 &&
                    <div className="square remaining half"><div className="bg"/></div>
                }
            </div>
        </div>
    )
};

export default UsageStats;
